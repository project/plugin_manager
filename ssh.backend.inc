<?php

/**
 * @file
 * The SSH Backend for the plugin manager.
 */

/**
 * Implementation of user hook plugin_manager_backend.  Checks
 * if the requirements for the ssh backend are enabled.
 *
 * @return
 *  Return 'ssh' if the requirements are available.
 */
function ssh_plugin_manager_backend() {
  if (function_exists('ssh2_connect')) {
    return 'ssh';
  }
}

/**
 * Install the supplied files to the appropriate locations.
 *
 * @param $files
 *   The files to install.
 * @param $type
 *   Either 'module' or 'theme' to indicate where to install to.
 * @param $username
 *   Local FTP Username
 * @param $password
 *   Local FTP Password
 * @return
 *  TRUE if all the files were installed, FALSE otherwise.
 */

function ssh_plugin_manager_copy($files, $type, $username, $password) {
  // Figure out the type of files.
  if (!in_array($type, array('modules', 'themes'))) {
    return FALSE;
  }
  $dir = 'sites/all/'. drupal_strtolower($type);

  // Connect to the localhost via ssh.
  $connection = ssh2_connect('localhost');
  if ($connection == FALSE) {
    drupal_set_message(t('Could not connect to ssh on this host.'), 'error');
    return FALSE;
  }

  if (!ssh2_auth_password($connection, $username, $password)) {
    drupal_set_message(t('The supplied username/password combination '
                      .'was not accepted.'), 'error');
    return FALSE;
  }

  // Prepare the directories to use later.
  $extract_dir = file_directory_path() .'/plugin_manager_extraction';


  // Try to guess the ftp address for the drupal install.
  // Start by putting the entire address together and replacing \ with /.
  $local_path = rtrim(str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'] . base_path()), '/');

  // Process each of the files.
  foreach ($files AS $file) {
    if (trim($file, "\\/") != $file) {
      ssh2_exec($connection, "mkdir ". escapeshellarg("$local_path/$dir/$file"));
    }
    else {
      ssh2_scp_send($connection, "$extract_dir/$file", "$local_path/$dir/$file");
    }
  }

  return TRUE;
}