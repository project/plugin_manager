<?php

/**
 * @file
 * The FTP Backend for the plugin manager.
 */

/**
 * Implementation of user hook plugin_manager_backend.  Checks
 * if the requirements for the ftp backend are enabled.
 *
 * @return
 *  Return 'ftp' if the requirements are available.
 */
function ftp_plugin_manager_backend() {
  if (function_exists('ftp_connect') OR ini_get('allow_url_fopen')) {
    return 'ftp';
  }
}

/**
 * Install the supplied files to the appropriate locations.
 *
 * @param $files
 *   The files to install.
 * @param $type
 *   Either 'module' or 'theme' to indicate where to install to.
 * @param $username
 *   Local FTP Username
 * @param $password
 *   Local FTP Password
 * @return
 *  TRUE if all the files were installed, FALSE otherwise.
 */
function ftp_plugin_manager_copy($files, $type, $username, $password) {
  if (function_exists('ftp_connect')) {
    return ftp_plugin_manager_copy_library($files, $type, $username, $password);
  }
  return ftp_plugin_manager_copy_wrapper($files, $type, $username, $password);
}

/**
 * Install the supplied files to the appropriate locations using ftp without
 * the stream wrapper.
 *
 * @param $files
 *   The files to install.
 * @param $type
 *   Either 'module' or 'theme' to indicate where to install to.
 * @param $username
 *   Local FTP Username
 * @param $password
 *   Local FTP Password
 * @return
 *  TRUE if all the files were installed, FALSE otherwise.
 */
function ftp_plugin_manager_copy_library($files, $type, $username, $password) {
  // Figure out the type of files.
  if (!in_array($type, array('modules', 'themes'))) {
    return FALSE;
  }
  $dir = 'sites/all/'. drupal_strtolower($type);

  // Connect to the local ftp server.
  $connect = ftp_connect('localhost');
  if (!$connect) {
    drupal_set_message(t('No ftp server could be found.'), 'error');
    return FALSE;
  }

  // Login to the local ftp server.
  if (!@ftp_login($connect, $username, $password)) {
    drupal_set_message(t('Could not login to the ftp server.'), 'error');
    return FALSE;
  }

  // Try to guess the ftp address for the drupal install.
  // Start by putting the entire address together and replacing \ with /.
  $local_path = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'] . base_path());

  // Grab the last on off the end and explode it.
  $local_path = explode('/', rtrim($local_path, '/'));

  // Try to guess which how far in we are chrooted...
  foreach ($local_path AS $index => $value) {
    unset($local_path[$index]);
    if (ftp_nlist($connect, implode('/', $local_path) .'/'. $dir)) {
      $ftp_path = implode('/', $local_path) .'/'. $dir;
      break;
    }
  }

  // If we couldn't guess it, then quit.
  if (!isset($ftp_path)) {
    drupal_set_message(t('Could not guess the ftp directory for drupal.'), 'error');
    return FALSE;
  }

  // Prepare the directories to use later.
  $extract_dir = file_directory_path() .'/plugin_manager_extraction/';

  // Process each of the files.
  foreach ($files AS $index => $file) {
    if (trim($file, "\\/") != $file) {
      @ftp_mkdir($connect, "$ftp_path/$file");
    }
    else {
      @ftp_put($connect, "$ftp_path/$file", $extract_dir . $file, FTP_BINARY);
    }
  }
  return TRUE;
}

/**
 * Install the supplied files to the appropriate locations using the ftp
 * stream wrapper.
 *
 * @param $files
 *   The files to install.
 * @param $type
 *   Either 'module' or 'theme' to indicate where to install to.
 * @param $username
 *   Local FTP Username
 * @param $password
 *   Local FTP Password
 * @return
 *  TRUE if all the files were installed, FALSE otherwise.
 */
function ftp_plugin_manager_copy_wrapper($files, $type, $username, $password) {
  // Figure out the type of files.
  if (!in_array($type, array('modules', 'themes'))) {
    return FALSE;
  }
  $dir = 'sites/all/'. drupal_strtolower($type);

  // Write the common part of the url
  $ftp_base_dir = "ftp://". urlencode($username) .":". urlencode($password) ."@localhost/";

  if (!@is_dir($ftp_base_dir)) {
    drupal_set_message(t('The supplied username/password combination '
                      .'was not accepted, or no local ftp server is running.'),
                       'error');
    return FALSE;
  }

  // Try to guess the ftp address for the drupal install.
  // Start by putting the entire address together and replacing \ with /.
  $local_path = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'] . base_path());

  // Grab the last on off the end and explode it.
  $local_path = explode('/', rtrim($local_path, '/'));

  // Try to guess which how far in we are chrooted...
  foreach ($local_path AS $index => $value) {
    unset($local_path[$index]);
    if (is_dir($ftp_base_dir . implode('/', $local_path) .'/'. $dir)) {
      $ftp_path = $ftp_base_dir . implode('/', $local_path) .'/'. $dir;
      break;
    }
  }

  // If we couldn't guess it, then quit.
  if (!isset($ftp_path)) {
    drupal_set_message(t('Could not guess the ftp directory for drupal.'), 'error');
    return FALSE;
  }

  // Prepare the directories to use later.
  $extract_dir = file_directory_path() .'/plugin_manager_extraction/';

  // Process each of the files.
  foreach ($files AS $index => $file) {
    if (trim($file, "\\/") != $file) {
      @mkdir("$ftp_path/$file");
    }
    else {
      @copy($extract_dir . $file, "$ftp_path/$file");
    }
  }
  return TRUE;
}
