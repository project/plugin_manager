<?php

/**
 * @file
 * The Forms used by the plugin manager.
 */

/**
 * FAPI for overview/search page.
 */
function plugin_manager_overview($form_state) {
  if (!plugin_manager_runnable()) {
    return array();
  }

  // If the repository is over 24 hours old then update it.
  if (variable_get("plugin_manager_last_reload", 0) + 86400 < date('U')) {
    drupal_set_message(t("The repository is out of date and will be updated."));
  }

  if (!isset($form_state['storage']['search'])) {
    $form['search_box'] = array(
      '#type' => 'fieldset',
      '#title' => t('Search'),
      '#collapsible' => FALSE,
    );
    $form['search_box']['search'] = array(
      '#type' => 'textfield',
    );
    $form['search_box']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
    );
  }
  else {
    // If the repository is over 24 hours old then update it.
    plugin_manager_reload();

    // Get a list of queued changes.
    $queue = variable_get("plugin_manager_queue", array());

    // Get a list of the non-installed plugins from the category.
    $result = db_query(
                "SELECT title, pmr.short_name AS short_name
                 FROM {plugin_manager_repository} pmr
                 LEFT JOIN(
                   SELECT DISTINCT short_name
                   FROM {plugin_manager_files}
                 ) pmf
                 ON(pmf.short_name = pmr.short_name)
                 WHERE title LIKE '%%%s%%'
                 AND pmf.short_name IS NULL
                 ORDER BY title", $form_state['storage']['search']);

    while ($row = db_fetch_array($result)) {
      $form[$row['short_name']] = array(
        '#type' => 'checkbox',
        '#name' => $row['short_name'],
        '#title' => htmlentities($row['title']),
        '#default_value' => isset($queue[$row['short_name']]),
      );
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Queue for Installation'),
    );
  }
  return $form;
}

function plugin_manager_overview_submit($form, &$form_state) {
  if (!isset($form_state['storage'])) {
    $form_state['storage']['search'] = $form_state['values']['search'];
    return;
  }
  else {
    plugin_manager_category_form_submit($form, $form_state);
  }
}

/**
 * Displays the modules in a selected category.
 *
 * @return
 * HTML output of the category page.
 */
function plugin_manager_modules() {
  plugin_manager_reload();
  // If we are looking at modules, then show the categories
  $output = '';
  $output .= drupal_get_form('plugin_manager_category_show');

  $func_args = arg();
  $category = count($func_args) - 1;
  if ($func_args[$category] != 'modules') {
    $output .= drupal_get_form('plugin_manager_category_form');
  }
  return $output;
}

/**
 * FAPI definition for the category list
 *
 * @ingroup forms
 */
function plugin_manager_category_show() {
  $func_args = arg();
  $category_id = count($func_args) - 1;
  $category = $func_args[$category_id];

  $form['categories'] = array(
    '#type' => 'fieldset',
    '#title' => t('Categories'),
    '#collapsible' => ($category != 'modules'),
    '#collapsed' => ($category != 'modules'),
  );

  $result = db_query("SELECT tag
                      FROM {plugin_manager_taxonomy}
                      WHERE tag NOT IN('Modules', 'Themes', 'Translations')
                      GROUP BY tag
                      ORDER BY tag");
  while ($row = db_fetch_array($result)) {
    $form['categories'][] = array(
      '#value' => l($row['tag'], "admin/build/plugin_manager/modules/". str_replace('/', '_', $row['tag'])) ."<br />",
    );
  }

  return $form;
}

/**
 * FAPI definition for the items in a selected category.
 *
 * @ingroup forms
 * @see plugin_manager_category_form_submit()
 */
function plugin_manager_category_form() {
  // If the repository is over 24 hours old then update it.
  plugin_manager_reload();

  // Get the category that provided.
  $func_args = arg();
  $category_id = count($func_args) - 1;
  $category = str_replace('_', '/', $func_args[$category_id]);

  // Get a list of queued changes.
  $queue = variable_get("plugin_manager_queue", array());

  // Get a list of the non-installed plugins from the category.
  $result = db_query(
              "SELECT title, pmr.short_name AS short_name
               FROM {plugin_manager_repository} pmr
               INNER JOIN {plugin_manager_taxonomy} pmt
               ON pmt.short_name = pmr.short_name
               LEFT JOIN(
                 SELECT DISTINCT short_name
                 FROM {plugin_manager_files}
               ) pmf
               ON(pmf.short_name = pmr.short_name)
               WHERE tag = '%s'
               AND pmf.short_name IS NULL
               ORDER BY title", $category);

  while ($row = db_fetch_array($result)) {
    $form[$row['short_name']] = array(
      '#type' => 'checkbox',
      '#name' => $row['short_name'],
      '#title' => htmlentities($row['title']),
      '#default_value' => isset($queue[$row['short_name']]),
    );
  }
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Queue for Installation'),
  );

  return $form;
}

/**
 * Callback plugin_manager_category_form.
 */
function plugin_manager_category_form_submit($form, &$form_state) {
  // See what is already queued.
  $queue = variable_get("plugin_manager_queue", array());

  // Go through each value in the list.
  foreach ($form_state['values'] AS $theme => $value) {
    if ($value == 1) {
      // Add it to the queue if needed.
      $queue[$theme] = $theme;
    }
    else {
      // Remove it from the queue otherwise.
      unset($queue[$theme]);
    }
  }

  // Set the new queue equal to our changed queue.
  variable_set("plugin_manager_queue", $queue);
  drupal_set_message("The queue has been updated.");
}

/**
 * FAPI for the first page of the installer
 *
 * @ingroup forms
 * @see plugin_manager_install_1()
 */
function plugin_manager_page_1() {
  // Get the total list and grab the release_history for each and set the download links.
  $queue = variable_get("plugin_manager_queue", array());
  $queue_info = plugin_manager_get_release_history($queue);
  variable_set("plugin_manager_queue_info", $queue_info);

  // Put a version drop down for each of the files
  foreach ($queue_info AS $project => $info) {
    $form[$project .'_version'] = array(
      '#title' => t('%title version', array('%title' => $info['title'])),
      '#type' => 'select',
      '#options' => $info['version'],
    );
  }

  // Put the continue button up.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue to Step 2'),
  );
  return $form;
}

/**
 * FAPI for the second page of the installer
 *
 * @ingroup forms
 * @see plugin_manager_install_2()
 */
function plugin_manager_page_2($form_state) {
  // On to page two.
  $queue_info = variable_get("plugin_manager_queue_info", array());

  $link_attr['attributes']['target'] = 'blank';

  // Make each one of the md5sum boxes.
  foreach ($queue_info AS $name => $plugin) {
    $version = $form_state['storage'][1][$name .'_version'];
    $link = $plugin['release'][$version]['release_link'];
    $form[$name .'_md5sum'] = array(
      '#title' => t('%plugin_title md5_file hash (copy/paste from above or <a href="!uri">available here</a>)', array('%plugin_title' => $plugin['title'], '!uri' => url($link, $link_attr))),
      '#type' => 'textfield',
      '#size' => 32,
      '#prefix' => "<iframe src='$link' style='width:100%;height:260px;'></iframe>",
    );
  }

  // Put the continue button up.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue to Step 3'),
  );
  return $form;
}

/**
 * FAPI for the third page of the installer
 *
 * @ingroup forms
 * @see plugin_manager_install_3()
 */
function plugin_manager_page_3($form_state) {
  $form['backend'] = array(
    '#title' => t('Installation method'),
    '#type' => 'select',
    '#options' => plugin_manager_backends(),
  );
  $form['username'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
  );
  $form['password'] = array(
    '#title' => t('Password'),
    '#type' => 'password',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Install'),
  );
  return $form;
}

/**
 * A page to list all of the queued changes and begin installation of them.
 */
function plugin_manager_install_form($form_state) {
  // Make sure that the plugin manager can be run.
  if (!plugin_manager_runnable()) {
    return array();
  }

  // If there aren't any, then just tell the just that.  And quit.
  $queue = variable_get("plugin_manager_queue", array());
  if (empty($queue)) {
    drupal_set_message("Nothing has been selected to install.", "error");
    return array();
  }

  // Allow the choice of versions on the first page.
  if (!isset($form_state['storage']['page'])) {
    return plugin_manager_page_1();
  }
  elseif ($form_state['storage']['page'] == 2) {
    return plugin_manager_page_2($form_state);
  }
  else {
    return plugin_manager_page_3($form_state);
  }
}

/**
 * Process the first page of the installer, by downloading the needed files.
 */
function plugin_manager_install_1($form, &$form_state) {
  // Download the files.
  $files = array();
  $queue_info = variable_get("plugin_manager_queue_info", array());
  foreach ($queue_info AS $name => $plugin) {
    $version = $form_state['storage'][1][$name .'_version'];
    $link = $plugin['release'][$version]['download_link'];
    $files[$name] = plugin_manager_get($link);

    // Check for an error.
    if ($files[$name] == FALSE) {
      drupal_set_message(t('Could not download @name', array('@name' => $name)), 'error');
      return;
    }
  }
  variable_set("plugin_manager_downloaded_files", $files);

  // Proceed to the next page.
  $form_state['storage']['page'] = 2;
  return;
}

/**
 * Process the second page of the installer by comparing calculated md5sums
 * to the md5sums supplied by the user.
 */
function plugin_manager_install_2($form, &$form_state) {
  $form_state['storage'][2] = $form_state['values'];
  // Compare the calculated and supplied md5sums.
  $files = variable_get("plugin_manager_downloaded_files", array());

  foreach ($files AS $name => $file) {
    $md5 = $form_state['storage'][2][$name .'_md5sum'];
    if (md5_file($file) != $md5) {
      drupal_set_message(t('The md5sum is incorrect for ') . $plugin['title'], 'error');
      $form_state['storage']['page'] = 2;
      return;
    }
  }

  $form_state['storage']['page'] = 3;
  return;
}

/**
 * Process the third page of the installer.  Extract, install, log and
 * clean up the files.
 */
function plugin_manager_install_3($form, &$form_state) {
  // Load the appropriate backend.
  $backend_list = plugin_manager_backends();
  $backend = $backend_list[$form_state['values']['backend']];
  $form_state['storage']['page'] = 3;

  // Go through each on of the files.
  $plugins = variable_get("plugin_manager_downloaded_files", array());
  foreach ($plugins AS $name => $plugin) {
    // Extract the files
    $files = plugin_manager_untar($plugin);

    // Find out whether the thing is a module or theme.
    $row = db_fetch_array(
      db_query("
        SELECT lower(tag) AS tag
        FROM {plugin_manager_taxonomy}
        WHERE short_name = '%s'
        AND tag IN('Modules', 'Themes')", $name));

    // Copy the extracted files
    $copy = call_user_func_array($backend .'_plugin_manager_copy',
      array($files, $row['tag'], $form_state['values']['username'],
        $form_state['values']['password']));

    foreach (array_reverse($files) AS $file) {
      // Mark files as installed
      if ($copy) {
        db_query("INSERT INTO {plugin_manager_files} VALUES('%s', '%s')",
          $name, $row['tag'] .'/'. $file);
      }

      // Remove the extracted files.
      $file = file_directory_path() .'/plugin_manager_extraction/'. $file;
      if (is_dir($file)) {
        rmdir($file);
      }
      else {
        unlink($file);
      }
    }

    // If it failed, stop now.
    if (!$copy) {
      drupal_set_message(t("Unable to install ") . $name, 'error');
      return;
    }

    drupal_set_message(t("Successfully installed ") . $name);
    unlink($plugin);
  }
  variable_set("plugin_manager_queue_info", array());
  variable_set("plugin_manager_queue", array());
}

/**
 * Process the data presented in the plugin_manager_install form.
 */
function plugin_manager_install_form_submit($form, &$form_state) {
  // If page one was submitted, download the requested files.
  if (!isset($form_state['storage']['page']) OR ($form_state['storage']['page'] == 1)) {
    $form_state['storage'][1] = $form_state['values'];
    return plugin_manager_install_1($form, $form_state);
  }
  elseif ($form_state['storage']['page'] == 2) {
    return plugin_manager_install_2($form, $form_state);
  }
  return plugin_manager_install_3($form, $form_state);
}

/**
 * FAPI for the uninstall page.
 *
 * Shows a list of packages to the user, allowing them to mark packages for removal.
 * @ingroup forms
 * @see plugin_manager_uninstall_submit()
 */
function plugin_manager_uninstall_form($form_state) {
  $form['theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Themes'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['module'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Get a list of the non-installed plugins from the category.
  $result = db_query(
              "SELECT name, type, COALESCE(title, name) AS title
               FROM {plugin_manager_files} pmf
               LEFT JOIN {system} s ON(pmf.short_name = s.name)
               LEFT JOIN {plugin_manager_repository} pmr ON(pmr.short_name = pmf.short_name)
               WHERE status = 0
               GROUP BY type, name
               ORDER BY type, name");

  while ($row = db_fetch_array($result)) {
    $form[$row['type']][$row['name']] = array(
      '#type' => 'checkbox',
      '#name' => $row['name'],
      '#title' => htmlentities($row['title']),
    );
  }
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Uninstall'),
  );
  return $form;
}

/**
 * Remove package files from the server.
 */
function plugin_manager_uninstall_form_submit($form, &$form_state) {
  foreach ($form_state['values'] AS $plugin => $value) {
    // If it isn't supposed to be removed, then skip it.
    if ($value != 1) {
      continue;
    }

    // Get the files to remove.
    $result = db_query("
      SELECT file_path
      FROM {plugin_manager_files}
      WHERE short_name = '%s'
      ORDER BY file_path DESC", $plugin);

    // Remove the files / folders requested.
    while ($row = db_fetch_array($result)) {
      $file = $_SERVER['DOCUMENT_ROOT'] . base_path() .'sites/all/'. $row['file_path'];

      if (!file_exists($file)) {
        drupal_set_message(t("@file does not seem to exist.", array('@file' => $file)), 'error');
        //continue;
      }
      elseif (is_dir($file)) {
        if (!@rmdir($file)) {
          drupal_set_message(t("Could not remove @file.", array('@file' => $file)), 'error');
          continue;
        }
      }
      else {
        if (!@unlink($file)) {
          drupal_set_message(t("Could not remove @file.", array('@file' => $file)), 'error');
          continue;
        }
      }
      db_query("DELETE FROM {plugin_manager_files}
                WHERE file_path = '%s'", $row['file_path']);
    }
  }
}
